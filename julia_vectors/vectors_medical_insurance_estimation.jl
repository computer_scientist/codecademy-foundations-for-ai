"""
    estimate_insurance_cost(name::String, age::Int, sex::Int, bmi::Float64, num_of_children::Int, smoker::Int)::Float64

Calculate the estimated insurance cost based on demographic and health characteristics.

# Arguments
- `name::String`: The name of the person for whom the insurance cost is being estimated.
- `age::Int`: The age of the person in years.
- `sex::Int`: The biological sex of the person, where typically 0 might represent female and 1 might represent male (depending on data convention).
- `bmi::Float64`: The Body Mass Index of the person.
- `num_of_children::Int`: The number of children the person has.
- `smoker::Int`: Smoking status of the person, where 0 is non-smoker and 1 is smoker.

# Returns
- `Float64`: The estimated annual insurance cost in dollars.

# Example
```julia
julia> estimate_insurance_cost("John Doe", 30, 1, 22.2, 0, 0)
"John Doe's Estimated Insurance Cost: 5000 dollars."
```
"""
function estimate_insurance_cost(name::String, age::Int, sex::Int, bmi::Float64, num_of_children::Int, smoker::Int)::Float64
    estimated_cost::Float64 = 250 * age - 128 * sex + 370 * bmi + 425 * num_of_children + 24_000 * smoker - 12_500
    
    println("$(name)'s Estimated Insurance Cost: $(estimated_cost) dollars.")
    
    return estimated_cost
end

function main()
    # Estimate insurance costs for Maria, Rohan, and Valentina
    maria_insurance_cost::Float64 = estimate_insurance_cost("Maria", 31, 0, 23.1, 1, 0)
    rohan_insurance_cost::Float64 = estimate_insurance_cost("Rohan", 25, 1, 28.5, 3, 0)
    valentina_insurance_cost::Float64 = estimate_insurance_cost("Valentina", 53, 0 , 31.4, 0, 1)
    
    # Actual insurance cost data (for comparison purposes
    names::Vector{String} = ["Maria", "Rohan", "Valentina"]
    insurance_costs::Vector{Float64} = [4_150.0, 5_320.0, 35_210.0]
    
    # Use List comprehension to mimic a zip like function in other languages and display results
    insurance_data::Vector{Tuple{String, Float64}} = [(names[i], insurance_costs[i]) for i in 1:length(names)]
    println("Here is the actual insurance cost data: $insurance_data")
    
   # Get Estimated insurace cost data and display results 
    estimated_insurance_data::Vector{Tuple{String, Float64}} = [
        ("Maria", maria_insurance_cost),
        ("Rohan", rohan_insurance_cost),
        ("Valentina", valentina_insurance_cost)
    ]
    println("Here is the estimated insurance cost data: $estimated_insurance_data")
end

# Call the main function
main()