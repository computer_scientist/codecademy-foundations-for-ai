# Define names and insurance costs
names::Vector{String} = ["Mohamed", "Sara", "Xia", "Paul", "Valentina", "Jide", "Aaron", "Emily", "Nikita", "Paul"]
insurance_costs::Vector{Float64} = [13262.0, 4816.0, 6839.0, 5054.0, 14724.0, 5360.0, 7640.0, 6072.0, 2750.0, 12064.0]

# Append additional data
push!(names, "Priscilla")
push!(insurance_costs, 8320.0)

# Create medical records as a list of tuples
medical_records::Vector{Pair{String, Float64}} = [name => cost for (name, cost) in zip(names, insurance_costs)]
println(medical_records)

# Calculate the number of medical records
num_medical_records::Int64 = length(medical_records)
println("There are $num_medical_records medical records.")

# Access the first medical records
first_medical_record::Pair{String, Float64} = medical_records[1]
println("Here is the first medical record: $first_medical_record")

# Sort medical records by insurance costs
sorted_medical_records::Vector{Pair{String, Float64}} = sort(medical_records, by = x -> x[2])
println("Here  are the medical records sorted by insurance cost: $sorted_medical_records")

# Extract the three cheapest and priciest records
cheapest_three::Vector{Pair{String, Float64}} = sorted_medical_records[1:3]
println("Here are the three cheapest insurance costs in our medical records: $cheapest_three")

priciest_three::Vector{Pair{String, Float64}} = sorted_medical_records[end-2:end]
println("Here are the three most expensive insurance costs in our medical records: $priciest_three")

# Count occurrences of the name "Paul"
occurrences_paul::Int64 = count(name -> name == "Paul", names)
println("Thre are $occurrences_paul individuals with the name Paul in our medical records.")